## Welcome!

I created this personal README to help you learn more about me and my working style. Consider it a living document and please do share feedback on it, especially if information that is interesing to you, is missing. 


* [🦊 My GitLab Handle - @alasch](https://gitlab.com/alasch)

## Hello, I'm Anne 👋

* I am originally from a small town in the [Ore Mountains](https://www.google.com/search?q=ore+mountains+germany&source=lnms&tbm=isch&sa=X&ved=2ahUKEwi0idjauMDsAhWC3KQKHReND4IQ_AUoAnoECAUQBA&biw=1410&bih=629) in Germany and currently reside in Munich. I consider myself a citizen of the world and try to live in different countries for extended periods. I lived in Atlanta, GA for 13 months, Bali and Sri Lanka for 6+ months each.
* **I love to travel and explore different cultures and places**. There is a long list of countries I plan to visit in the upcoming years. 
    * My top 3 countries that I haven’t visited, yet, are Brazil, Colombia, Morocco.  
    * You can easily find out through [my Instagram](https://instagram.com/anneanda_) where I currently am. 
* I feel most alive when I am by the ocean. **My dream is to have a home by the ocean** one day. I enjoy any water-related activity, mostly surfing, scuba diving, or just walking along the beach. 


#### What else is good to know?
* **I am a passionate yogi and movement practitioner**. I may do random stretches while we are on a Zoom call (I am still listening though).  
    * I am certified to teach various styles of yoga, including Vinyasa, Yin, and Aerial. I don’t teach much at the moment, but my daily routine includes yoga in the morning and a few Yin poses before sleeping. 
* **Health, be it physical, emotional, or mental, is important to me**. I am in general quite reflective and daily meditation supports me with that. I enjoy learning more about how our bodies work and what helps to stay healthy. 
* **I am an [outgoing introvert](https://iheartintelligence.com/reveal-outgoing-introvert/#:~:text=Outgoing%20introverts%20are%20the%20type,friends%20is%20a%20true%20asset.)** which shows in many ways. For instance, I love to go out, experience new things and learn about people - but find all that tiring and need to make sure I have enough moments to "recover" and recharge.
* I am currently training to become a life and business coach which connects me back to Psychology which I have a degree in.  


## My role

I am a Staff UX Researcher covering various product areas. Prior to that I have been a Sr. UX Researcher for the Manage and Plan stages, and UX Research Manager covering the Dev section.

I have been doing user research since 2008 and have a background in Psychology and Human Factors. Before GitLab I have done lots of [automotive research at HERE](https://360.here.com/2015/04/08/jaguar-land-rover-here-auto/) as well as in the [privacy and security space at Google](https://myaccount.google.com). 


## How I work

* **I am based in CET and like to start around 9 am**. I am an early bird and use this time mostly for focused work. I am significantly more productive then.
* **Meetings often start around 3 pm which is the time I am slowly getting tired**. I am still focused when speaking to you and will also take information in. However, I might be quieter and use the next morning to think through what we discussed. It’s not uncommon that I come back to you then with a better idea. 
* **I take several smaller breaks during the day** and a larger one to go for a walk outside. My calendar is very transparent about my whereabouts and you can always schedule time on any of the free slots. If you don’t find any, let me know and I will make time. 
* **I check emails a few times a day and Slack is always on during work time**. Please ping me there when you need something urgent. I don’t have Slack or email on my phone. 
* **GitLab is the first time where I am working 100% remotely**. I am fully embracing to work asynchronous and since it's new I know I will make mistakes. I am always open to receive direct feedback when I could have done something differently or embody GitLab’s values differently.  


## Working with you

* I always assume positive intent.
* I value direct feedback and am happy to receive it as well as provide it.  
* I value your time as much as mine and therefore, aim to respond to you quickly and be transparent about delays. I love due dates and appreciate it if you tell me by when you need something from me (otherwise I will ask).
* I believe everyone is capable of finding solutions to the problems they face. And I am happy to support you finding them. (Says the coach side of me 😁)


## Strengths and quirks

Culturally it's hard for me to brag about my strengths. As a German, you are taught to be modest and consider everything a team effort. I am getting better at talking about what I do well, so here we go:
* **I am passionate and stronger in qualitative research methods**. At the same time, statistics and quantivative methods don't scare me away. I am pretty fit with those as well. 
* **I love setting up plans** to transform chaos into order and I hold myself accountable to deliver on time. 
* **I am very pragmatic**. This also means I am flexible to deviate from a plan (including my own) or specified process when necessary.
 * **I am an excellent listener** and always have an open ear, be it work or personal matters. 

On my quirky side
* **I talk a lot** about the things I am passionate about. At times too much. Please stop me if you feel uncomfortable or unheard. 
* **I can speak very fast** and provide a lot of detail at once. I am actively trying to get better at it, so please tell me if I am too fast. For what it's worth, talking is a way to structure my thoughts and I might share with you unfiltered, messy thoughts to gain clarity. 
* **Health is my priority, but my flexibility and pragmatism sometimes lead to violating my boundaries when it comes to healthy habits**. If my work is all over the place or you see any other reason why I could be out of balance, kindly remind me to prioritize health.  
* **German is my native language** and I blame it for my at times complex sentence structure (and accent) when speaking or writing in English. 

According to [this personality test](https://www.16personalities.com/), I am an Assertive Advocate (INFJ-A) that can be described as folks where "success doesn't come from money or status, but from seeking fulfillment, helping others, and being a force for good in the world." Take a look [here](https://docs.google.com/presentation/d/161uxVUpY7ft_EL-2Z5nZogN4k3zR62_yUkBlupWVB1c/edit#slide=id.p) for more details.

<br>

##### Inspired by

I read too many ReadMe's to list them all individually. The [ones from the UX department](https://gitlab.com/aregnery/dear-journal/-/blob/master/README.md) served me greatly though and are recommended.  

